MONASH UNIVERSITY • CREATIVE CODING
Processing Sketches Version 1.4

These directories contain Processing sketches and associated files for the on-line course “Creative Coding” run by Monash University through the FutureLearn platform.

It also contains the individual exercises i.e code written by the owner of this repository (not provided by the course). 

http://www.futurelearn.com/courses/creative-coding
