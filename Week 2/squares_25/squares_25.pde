/*
 * Code to reproduce the pattern of 25 Squares, 1991 Vera Molnar 
 */

void setup() {
  size(600, 600);
  rectMode(CORNER);
  noStroke();
  frameRate(1);  // set the frame rate to 1 draw() call per second
  randomSeed(hour() + minute() + second() + millis()); // improve randomness 
}


void draw() {

  background(180); // clear the screen to grey
  
  int num = 5; // 25 squares (5x5)
  int base_gap = 5; // base gap set to 5, but different ones will be used when drawing
  
  
  // calculate the size of each square for the given number of squares and gap between them
  float cellsize = ( width - (num + 1) * base_gap ) / (float)num;
  
  // calculate shadow offset
  float offsetX = cellsize/16.0;
  float offsetY = cellsize/16.0;
 

    for (int i=0; i<num; i++) {
      for (int j=0; j<num; j++) {

        fill(100, 220); // shadow - might get better, there is room to improve
        int gap = (int) random(0, 7); // random gap
        rect(gap * (i+1) + cellsize * i + offsetX, gap * (j+1) + cellsize * j + offsetY, cellsize, cellsize);

        // if statement to generate brown or red squares (most of them should be brown)
        if(gap < 6) fill(165, 42, 42, 180); // brown
        else fill(255, 0, 0, 180); // red
        rect(gap * (i+1) + cellsize * i, gap * (j+1) + cellsize * j, cellsize, cellsize);
      }
    }
    // saveFrame(); // can be used to save each image generated
} //end of draw 

